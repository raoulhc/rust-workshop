#[derive(Debug)]
enum RawInstruction {
    Right,
    Left,
    Increment,
    Decrement,
    Output,
    Input,
    StartLoop,
    EndLoop,
}

// use RawInstruction::*;

impl RawInstruction {
    fn from_char(chr: char) -> Option<RawInstruction> {
        match chr {
            '>' => Some(RawInstruction::Right),
            '<' => Some(RawInstruction::Left),
            '+' => Some(RawInstruction::Increment),
            '-' => Some(RawInstruction::Decrement),
            '.' => Some(RawInstruction::Output),
            ',' => Some(RawInstruction::Input),
            '[' => Some(RawInstruction::StartLoop),
            ']' => Some(RawInstruction::EndLoop),
            _ => None
        }
    }
}

#[derive(Debug)]
struct Instruction {
    line: usize,
    col: usize,
    instr: RawInstruction,
}

fn parse_from_file(
    input: &str)
    -> Result<Vec<Instruction>, Box<std::error::Error>> {
    let input = std::fs::read_to_string(input)?;
    Ok(input
       .lines()
       .enumerate()
       .flat_map(|(line, s)| {
           s.chars().enumerate()
               .flat_map(move |(col, c)| RawInstruction::from_char(c)
                         .map(|instr| Instruction {line, col, instr}))
       })
       .collect())
}

fn main() -> Result<(), Box<std::error::Error>> {
    let filename = "input-files/w1p2";
    let instructions = parse_from_file(filename)?;
    for instr in instructions.iter() {
        println!("[{}:{}:{}] {:?}", filename, instr.line, instr.col, instr.instr);
    }
    Ok(())
}
