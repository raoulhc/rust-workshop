// test reader
use core::fmt;
use std::io::{BufReader, BufRead};
use std::fs::File;
use std::collections::HashMap;

#[derive(Debug)]
enum Line {
    WithoutNum(String),
    WithNum(String, String)
}


impl From<String> for Line {
    fn from(x: String) -> Line {
        match x.rfind(":") {
            None => Line::WithoutNum(x.to_string()),
            Some(ix) => {
                let (name, score) = x.split_at(ix);
                let mut score = score.to_string();
                score.remove(0);
                Line::WithNum(name.to_string(), score)
            }
        }
    }
}

#[derive(Debug)]
struct Score {
    total: usize,
    tests: usize,
    missed: usize,
}

impl Default for Score {
    fn default() -> Score {
        Score {total: 0, tests: 0, missed: 0}
    }
}

impl fmt::Display for Score {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        write!(f, "{} test{}, total score of {}. {} tests were missed",
               self.tests, "s", self.total, self.missed)
    }
}

impl Score {
    fn add_score(&mut self, mark: usize) {
        self.total += mark;
        self.tests += 1;
    }

    fn missed_test(&mut self) {
        self.missed += 1;
    }
}

fn parse_file(filename: &str) -> Result<Vec<Line>, Box<std::error::Error>> {
    let reader = BufReader::new(File::open(filename)?);
    Ok(reader.lines().map(|x| Line::from(x.unwrap())).collect())
}

fn main() -> Result<(), Box<std::error::Error>> {
    let vec = parse_file("input-files/w2p1")?;
    let mut map : HashMap<&str, Score> = HashMap::new();
    for elt in vec.iter() {
        println!("{:?}", elt);
        match elt {
            Line::WithNum(name, score) => {
                let entry = map.entry(name).or_default();
                entry.add_score(score.parse::<usize>()?);
            },
            Line::WithoutNum(name) => {
                let entry = map.entry(name).or_default();
                entry.missed_test();
            }
        };
    }
    for (k, v) in map.iter() {
        println!("{} took {}", k, v);
    }
    Ok(())
}
