// Brainfuck stripper
use std::env;
use std::io::{BufReader, Read};
use std::fs::File;
use regex::Regex;

fn main() -> std::io::Result<()> {
    let input_string = match env::args().nth(1) {
        Some(x) => x,
        None => panic!("Something went wrong"),
    };

    let re = Regex::new(r"<|>|\+|-|\.|,|\[|\]").unwrap();
    let file = File::open(input_string)?;
    let mut buf_reader = BufReader::new(file);
    let mut buf = String::new();
    buf_reader.read_to_string(&mut buf)?;
    buf = buf.chars()
        .filter(|x| re.is_match(x.to_string().as_str()))
        .collect();
    println!("{}", buf);
    Ok(())
}
