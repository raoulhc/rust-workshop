use std::env;
use std::io::{BufReader, BufRead};
use std::fs::File;

fn main() -> std::io::Result<()> {
    assert_eq!(env::args().len(), 2);
    let input_string = match env::args().nth(1) {
        Some(x) => x,
        None => panic!("Something went wrong!")
    };

    let file = File::open(input_string)?;
    let buf_reader = BufReader::new(file);
    let mut count : i32 = 0 ;
    for line in buf_reader.lines() {
        count += line?.parse::<i32>().unwrap();
    }
    println!("{}", count);
    Ok(())
}
